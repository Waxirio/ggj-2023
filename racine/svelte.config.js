// import adapter from '@sveltejs/adapter-auto';
import adapter from '@sveltejs/adapter-static';
 
const config = {
  kit: {
    adapter: adapter({precompress:true}),
  }
};

export default config;